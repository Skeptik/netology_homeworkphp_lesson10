<!DOCTYPE html>
<html>
<head>
  <title>Домашнее задание: Классы и объекты</title>
  <style type="text/css">
    div { 
	  padding: 7px;
   	  padding-right: 20px; 
      border: solid 1px black;
   	  font-family: Verdana, Arial, Helvetica, sans-serif; 
   	  font-size: 13pt; 
   	  background: #E6E6FA;
   	}
   	body {
   	  background: #159445;
   	}
  </style>
</head>
<body>
  <div>
    <center><h1>Полиморфизм<h1></center>
	<p>
	  <strong>Полиморфизм</strong> - Единообразная работа с разными по своей природе сущностями, когда наследуемые классы или просто различные классы имеют одинаковые методы но с различной реализацией. Позволяет писать более абстрактные программы и повысить коэффициент повторного использования кода. Общие свойства объектов объединяются в систему, интерфейс, класс.
	</p>
  </div>

  <div>
    <center><h1>Наследование<h1></center>
    <p>
      <strong>Наследование</strong> - Позволяющий описать новый класс на основе уже существующего (родительского), при этом свойства и функциональность родительского класса заимствуются новым классом. Это позволяет обращаться с объектами класса-наследника точно так же, как с объектами базового класса.
    </p>
  </div>

  <div>
    <center><h1>Отличие интерфейсов и абстрактных классов<h1></center>
	<p>
	  Интерфейс - такой же абстрактный класс,только в нем не может быть свойств и не определены тела у методов. Абстрактный класс наследуется(etxends), а интерфейс реализуется (implements). Наследовать мы можем только 1 класс, а реализовать сколько угодно.
	</p>
	<p>
	  <strong>Когда следует использовать абстрактные классы</strong>
	  <ul>
	    <li>
	      Если надо определить общий функционал для родственных объектов
	    </li>
		<li>
		  Если нужно, чтобы все производные имели некоторую общую реализацию. Если мы захотим изменить базовый функционал во всех наследниках, то достаточно поменять его в абстрактном базовом классе.
		</li>
	  </ul>
	</p>
	<p>
	  <strong>Когда следует использовать абстрактные классы</strong>
	  <ul>
	    <li>
	  	  Если нам надо определить функционал для разных объектов, которые могут быть никак не связаны между собой.
	    </li>
	  </ul>
	</p>
  </div>
</body>
</html>

<?php 
class General
{
    public $date;
    public $country;
    public $color;
    public $price;

    public function setDate($date)
    {
        $this->date = $date;
    }
    public function setCountry($country)
    {
        $this->country = $country;		
    }
    public function setColor($color)
    {
        $this->color = $color;		
    }
    public function setPrice($price)
    {
        $this->price = $price;		
    }
    public function getDate()
    {
        return $this->date;		
    }
    public function getCountry()
    {
        return $this->country;		
    }
    public function getColor()
    {
        return $this->color;		
    }
    public function getPrice()
    {
        return $this->price;		
    }
}
echo '<center><h1>Машины</h1></center>';

interface AllCar
{
    public function openCowl();
}

class Car  extends General implements AllCar
{
    private $capacity;     //мощность
    private $maxSpeed;     // максимальная скорость
    private $wheel;        // кол колес
    private $typeBody;     // вид кузова

    public function __construct($color, $capacity, $maxSpeed, $wheel, $typeBody)
    {
        $this->color=$color;
        $this->capacity=$capacity;
        $this->maxSpeed=$maxSpeed;
        $this->wheel=$wheel;
        $this->typeBody=$typeBody;
	}
    public function openCowl()            //Открой капот
    {
        echo "Капот открыт<br>";
    }
    public function checkSpeed ($speed)  //Проверка, может ли машина двигаться по трассе
    {                                    //Ограничение передается в переменной $speed
        if($this->maxSpeed > $speed) {
            echo 'Машина предназначена для данной трассы, ее скорость '.$this->maxSpeed.'<br>';
        } else {
            echo 'Машина не предназначена для данной трассы, ее скорость '.$this->maxSpeed.'<br>';
        }
        echo 'Требуемая скорость '.$speed.'<br>';
    }
}

$bmw = new Car('black', 100, 300, 4, 'хэтчбек');
$audi = new Car('red', 120, 200, 4, 'седан');
$bmw->setDate('2018');
echo $bmw->getColor().'<br>';
$bmw->setColor('green');
echo $bmw->getDate().'<br>';
echo $bmw->getColor().'<br>';
$audi->openCowl();
$bmw->checkSpeed(250);

echo '<center><h1>Телевизоры</h1></center>';

interface AllTv
{
    public function onTv();
    public function offTv();
}

class Tv extends General implements AllTv
{
    private $diagonal;        //Диагональ
    private $format_3D;       //Поддерживает ли 3D формат
    private $typeCase;        //Вид корпуса

    public function __construct($diagonal, $format_3D, $price, $typeCase)
    {
        $this->diagonal=$diagonal;
        $this->format_3D=$format_3D;
        $this->price=$price;
        $this->$typeCase=$typeCase;
    }
    public function onTv()
    {
        echo "Включить телевизор.<br>";
    }
    public function offTv()
    {
        echo "Выключить телевизор.<br>";
    }
    public function check_3D()               //Проверка поддерживает ли 3D
    {
        if($this->format_3D) {
            echo 'Данная модель поддерживает 3D формат.<br>';
        } else {
            echo 'Данная модель не поддерживает 3D формат.<br>';
        }
    }
}

$samsung = new Tv(19, true, 10000, 'пластик');
$lg = new Tv(17, false, 5000, 'дерево');
$samsung->check_3D();
$lg->check_3D();
$lg->onTv();
$lg->offTv();
$lg->setCountry('USA');
echo $lg->getCountry();

echo '<center><h1>Шариковые ручки</h1></center>';

interface allPen
{
    public function beginWrite();
    public function stopWrite();
}

class BallPen extends General implements allPen
{
    private $typeCore;           //Вид стержня

    public function __construct($typeCore, $color, $price)
    {
        $this->typeCore=$typeCore;
        $this->color=$color;
        $this->price=$price;
    }
    public function beginWrite()
    {
        echo "Пишу";
    }
    public function stopWrite()
    {
        echo "Стоп";
    }
}

class ButtonPen extends BallPen
{
    public $button;

    public function getButton($butt)
    {
        $this->button = $butt;
    }
    public function checkButton()
    {
        if($this->button) {
            echo "Кнопочная.<br>";
        } else {
            echo "Без кнопки.<br>";
        }
    }
}

$penBlack = new BallPen('тонкий', 'черный', 100);
$penBlue = new BallPen('толстый', 'синий', 50);
echo $penBlack->getPrice().'<br>';
$penBlack->setPrice(150);
echo $penBlack->getPrice().'<br>';
echo $penBlue->getPrice().'<br>';
$butPen = new ButtonPen('толстый', 'синий', 500);
echo $butPen->getPrice().'<br>';
$butPen->getButton(true);
$butPen->checkButton();

echo '<center><h1>Утки</h1></center>';

interface AllDuck
{
    public function makeSound();
}

class Duck extends General implements AllDuck
{
    const MAXAGE = 10;                           //Максимальный возраст
    private static $quantityDuck = 0;            //Количество уток
    private $name;             					 //Имя
    private $gender;            				 //Пол
    private $colorBeak;        					 //Цвет клюва
    private $paws;             					 //Количество лап
    private $age;              					 //Возраст
    private $sound;                              
	
    public function __construct($name, $gender, $colorBeak, $paws, $age, $sound)
    {
        $this->name=$name;
        $this->gender=$gender;
        $this->colorBeak=$colorBeak;
        $this->paws=$paws;
        $this->age=$age;
        $this->sound=$sound;
        self::$quantityDuck++;
    }
    public function makeSound() {
        echo 'Я говорю '.$this->sound.'<br>';
    }
    public function checkDuck()                 //Проверка возраста утки
    {
        if($this->age <= self::MAXAGE) {
            echo "Утка молодая<br>";
        } else {
            echo "Утка старая<br>";
        }
    }
    public static function getQuantityDuck()
    {
        echo self::$quantityDuck.'<br>';
    }
}

$duck_1 = new Duck('First', 'мужской', 'красный', 2, 9, 'Кря');
$duck_2 = new Duck('Second', 'женский', 'желтый', 2, 10, 'Хрю');
$duck_3 = new Duck('Third', 'мужской', 'белый', 2, 11, 'Мяу');
$duck_1->checkDuck();
$duck_2->checkDuck();
$duck_3->checkDuck();
echo $duck_1::getQuantityDuck();
echo $duck_2::getQuantityDuck();
echo $duck_3::getQuantityDuck();
$duck_1->makeSound();
$duck_1->setColor('Фиолетовый');
echo $duck_1->getColor().'<br>';
$duck_2->makeSound();
$duck_3->makeSound();

echo '<center><h1>Товары Дополнительное задание</h1></center>';

interface AllProduct
{
    public function setName($name);
    public function setPrice($price);
    public function setCategory($category);
    public function getName();
    public function getPrice();
    public function getCategory();
}

interface DiscountProduct
{
    public function discountPrice();
}

trait Delivery
{
    public function delivery()
    {
        if($this->discPrice && $this->discPrice < $this->price) {
            echo 'Доставка '.$this->name.' 250<br>';
        } else {
            echo 'Доставка '.$this->name.' 300<br>';
        }
    }
}

class Product extends General implements AllProduct
{
    protected $name;
    protected $category;
    public function __construct($name, $price, $category)
    {
        $this->name=$name;
        $this->price=$price;
        $this->category=$category;
    }

    public function setName($name)
    {
        $this->name=$name;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setCategory($category)
    {
        $this->category;
    }
    public function getCategory()
    {
        return $this->category;
    }
}

class dairyProduct extends Product implements DiscountProduct
{
    private $discount;
    private $weight;
    private $discPrice;
    use Delivery;

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    public function discountPrice()
    {
        if($this->weight > 10 && $this->discount) {
            $this->discPrice = round($this->price-($this->price * $this->discount / 100), 2);
            return $this->discPrice;
        } else {
            return $this->price;
        }
    }
}

class HouseholdProduct extends Product implements DiscountProduct
{
    private $discount;
    private $discPrice;
    use Delivery;

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }
    public function discountPrice()
    {
        if($this->discount) {
            $this->discPrice = round($this->price-($this->price * $this->discount / 100), 2);
            return $this->discPrice;
        } else {
            return $this->price;
        }
    }
}

class VegetableProduct extends Product
{
    public function delivery()
    {
        echo 'Доставка '.$this->name.' 300<br>';	
    }
}

$cheese = new dairyProduct('Пармезан', 500, 'Молоный продукт');
$cheese->setDiscount(10);
$cheese->setWeight(50);
$cottageCheese = new dairyProduct('Творог', 200, 'Молоный продукт');
$cottageCheese->setDiscount(10);
$cottageCheese->setWeight(7);
echo 'Цена '.$cheese->getName().', без скидки - '.$cheese->getPrice().', со скидкой - '. $cheese->discountPrice().'<br>';
$cheese->delivery();
echo 'Цена '.$cottageCheese->getName().', без скидки - '
    .$cottageCheese->getPrice().', со скидкой - '. $cottageCheese->discountPrice().'<br>';
$cottageCheese->delivery();

$sponge = new HouseholdProduct('Губка', 40, 'Хоз. товар');
$sponge->setDiscount(10);
echo 'Цена '.$sponge->getName().', без скидки - '.$sponge->getPrice().', со скидкой - '. $sponge->discountPrice().'<br>';
$sponge->delivery();
$fairy = new HouseholdProduct('Моющее Fairy', 150, 'Хоз. товар');
echo 'Цена '.$fairy->getName().', без скидки - '.$fairy->getPrice().', со скидкой - '. $fairy->discountPrice().'<br>';
$fairy->delivery();

$pizza = new VegetableProduct('Пицца', 300, 'Выпечка');
echo 'Цена '.$pizza->getName().' - '.$pizza->getPrice().'<br>';
$pizza->delivery();
?>